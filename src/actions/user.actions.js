import { userConstants } from '../constants'
import { userService } from '../services'
import { alertActions } from './'
import { history } from '../helpers'

export const userActions = {
    login,
    logout
}

function login(identifier, password) {
    return dispatch => {
        dispatch(request({ identifier }))

        userService.login(identifier, password).then(
            response => {
                userService.getMe().then(
                    responseUser => {
                        dispatch(success(responseUser))
                        history.push('/')
                    },
                    errorUser => {
                        dispatch(failure(errorUser))
                        dispatch(alertActions.error(errorUser))
                    }
                )
            },
            error => {
                dispatch(failure(error))
                dispatch(alertActions.error(error))
            }
        )
    }

    function request(user) {
        return { type: userConstants.LOGIN_REQUEST, user }
    }
    function success(user) {
        return { type: userConstants.LOGIN_SUCCESS, user }
    }
    function failure(error) {
        return { type: userConstants.LOGIN_FAILURE, error }
    }
}

function logout() {
    userService.logout()
    return { type: userConstants.LOGOUT }
}
