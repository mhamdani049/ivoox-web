import React, { Component } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'

import { history } from './helpers'
import PrivateRoute from './components/PrivateRoute'

import RouterHome from './components/home/router'
import RouterAuth from './components/auth/router'
import RouterDashboard from './components/dashboard/router'

class App extends Component {
    constructor(props) {
        super(props)
    }

    render() {
        return (
            <Router history={history}>
                <Switch>
                    <Route exact path="/" component={RouterHome} />
                    <Route exact path="/Auth" component={RouterAuth} />
                    <PrivateRoute
                        exact
                        path="/Dashboard"
                        component={RouterDashboard}
                    />
                </Switch>
            </Router>
        )
    }
}

function mapStateToProps(state) {
    const { alert } = state
    return {
        alert
    }
}

const connectedApp = connect(mapStateToProps)(App)
export default connectedApp
