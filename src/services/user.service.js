import { authHeader } from '../helpers'

const apiUrl = 'http://assa.code.aksimaya.id/service-request/api'

export const userService = {
    login,
    logout,
    getMe
}

function login(identifier, password) {
    const requestOptions = {
        method: 'POST',
        headers: { 'Content-Type': 'application/json' },
        body: JSON.stringify({ identifier, password })
    }

    return fetch(`${apiUrl}/user/token`, requestOptions)
        .then(handleResponse)
        .then(response => {
            localStorage.setItem(
                'AmanahIvooxWebClient_token',
                JSON.stringify(response.token)
            )

            return response
        })
}

function logout() {
    localStorage.removeItem('AmanahIvooxWebClient_token')
    localStorage.removeItem('AmanahIvooxWebClient_user')
}

function getMe() {
    const requestOptions = {
        method: 'GET',
        headers: authHeader()
    }

    return fetch(`${apiUrl}/user`, requestOptions)
        .then(handleResponse)
        .then(response => {
            localStorage.setItem(
                'AmanahIvooxWebClient_user',
                JSON.stringify(response.data)
            )

            return response
        })
}

function handleResponse(response) {
    return response.text().then(text => {
        const data = text && JSON.parse(text)
        if (!response.ok) {
            if (response.status === 401) {
                logout()
                window.location.reload(true)
            }

            const error = (data && data.message) || response.statusText
            return Promise.reject(error)
        }

        return data
    })
}
