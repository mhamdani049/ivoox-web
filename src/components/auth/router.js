import React from 'react'
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom'

import Index from './'

function RouterAuth() {
    return (
        <Router>
            <div>
                <Switch>
                    <Route exact path="/auth" component={Index} />
                </Switch>
            </div>
        </Router>
    )
}

export default RouterAuth
