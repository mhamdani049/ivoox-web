import React from 'react'
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom'

import Index from './'

function RouterDashboard() {
    return (
        <Router>
            <div>
                <Switch>
                    <Route exact path="/dashboard" component={Index} />
                </Switch>
            </div>
        </Router>
    )
}

export default RouterDashboard
