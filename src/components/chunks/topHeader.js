import React, { Component } from 'react'

class TopHedaer extends Component {
    render() {
        return (
            <div className="container">
                <div className="flex-parent">
                    <div className="flex-child" id="topbar-ivoox">
                        <div className="row">
                            <div className="col-md-6 col-6" id="topbar-date">
                                <p>15-02-2019</p>
                            </div>

                            <div className="socials socials--nobase socials--dark justify-content-end col col-6">
                                <a
                                    className="social social-facebook"
                                    href="#"
                                    target="_blank"
                                    aria-label="facebook"
                                >
                                    <i className="ui-facebook" />
                                </a>
                                <a
                                    className="social social-twitter"
                                    href="#"
                                    target="_blank"
                                    aria-label="twitter"
                                >
                                    <i className="ui-twitter" />
                                </a>
                                <a
                                    className="social social-google-plus"
                                    href="#"
                                    target="_blank"
                                    aria-label="google"
                                >
                                    <i className="ui-google" />
                                </a>
                                <a
                                    className="social social-youtube"
                                    href="#"
                                    target="_blank"
                                    aria-label="youtube"
                                >
                                    <i className="ui-youtube" />
                                </a>
                                <a
                                    className="social social-instagram"
                                    href="#"
                                    target="_blank"
                                    aria-label="instagram"
                                >
                                    <i className="ui-instagram" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default TopHedaer
