import React, { Component } from 'react'

class Footer extends Component {
    render() {
        return (
            <footer className="footer footer--white">
                <hr />
                <div className="container">

                    {/* Ad Banner 728 */}
                    {/* <div className="text-center">
                        <a href="#">
                            <img className="logo__img" src={require('../../assets/img/content/placeholder_728.jpg')} />
                        </a>
                    </div> */}

                    {/* <hr /> */}
                    <div className="footer__widgets footer__widgets--short">
                        <div className="row">

                            <div className="col-lg-3">
                                <a href="index.html" className="logo" id="logo-footer-ivoox">
                                    <img className="logo__img" src={require('../../assets/img/logo-ivoox.png')} />
                                </a>
                            </div>

                            <div className="col-lg-3 col-md-6">
                                <aside className="widget widget_nav_menu">
                                    <h4 className="widget-title">Useful Links</h4>
                                    <ul>
                                        <li><a href="about.html">About</a></li>
                                        <li><a href="contact.html">News</a></li>
                                        <li><a href="categories.html">Advertise</a></li>
                                        <li><a href="shortcodes.html">Support</a></li>
                                        <li><a href="shortcodes.html">Features</a></li>
                                        <li><a href="shortcodes.html">Contact</a></li>
                                    </ul>
                                </aside>
                            </div>

                            <div className="col-lg-3 col-md-6">
                                <aside className="widget widget_nav_menu">
                                    <h4 className="widget-title">Useful Links</h4>
                                    <ul>
                                        <li><a href="about.html">About</a></li>
                                        <li><a href="contact.html">News</a></li>
                                        <li><a href="categories.html">Advertise</a></li>
                                        <li><a href="shortcodes.html">Support</a></li>
                                        <li><a href="shortcodes.html">Features</a></li>
                                        <li><a href="shortcodes.html">Contact</a></li>
                                    </ul>
                                </aside>
                            </div>

                            <div className="col-lg-3">
                                <div className="socials socials--large socials--rounded socials--grey justify-content-lg-end">
                                    <a href="#" className="social social-facebook social--large social--rounded" aria-label="facebook"><i className="ui-facebook"></i></a>
                                    <a href="#" className="social social-twitter social--large social--rounded" aria-label="twitter"><i className="ui-twitter"></i></a>
                                    <a href="#" className="social social-google-plus social--large social--rounded" aria-label="google+"><i className="ui-google"></i></a>
                                    <a href="#" className="social social-youtube social--large social--rounded" aria-label="youtube"><i className="ui-youtube"></i></a>
                                    <a href="#" className="social social-instagram social--large social--rounded" aria-label="instagram"><i className="ui-instagram"></i></a>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div className="footer__bottom top-divider">
                        <div className="row">
                            <div className="col-lg-6">
                                <p>
                                    Copyright PT. Media Nusantara Sakti @ 2016
                                </p>
                                <p>Powered by <a href="http://asclar.co.id/" target="_blank" rel="noopener">ASCLAR Indonesia</a></p>
                            </div>
                            <div className="col-lg-6">
                                <div className="apps-footer row">
                                    <div className="col-md-4 col-4">
                                        {/* <img src="img/comodo-footer.png" /> */}
                                        <img className="logo__img" src={require('../../assets/img/comodo-footer.png')} />
                                    </div>
                                    <div className="col-md-4 col-4">
                                        {/* <img src="img/comodo-footer.png" /> */}
                                        <img className="logo__img" src={require('../../assets/img/comodo-footer.png')} />
                                    </div>
                                    <div className="col-md-4 col-4">
                                        {/* <img src="img/comodo-footer.png" /> */}
                                        <img className="logo__img" src={require('../../assets/img/comodo-footer.png')} />
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </footer>
        )
    }
}

export default Footer
