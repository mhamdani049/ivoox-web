import React, { Component } from 'react'

class Trending extends Component {
    render() {
        return (
            <div className="container">
                <div className="trending-now trending-now--1">
                    <span className="trending-now__label">
                        <i className="ui-flash" />
                        <span className="trending-now__text d-lg-inline-block d-none">
                            Newsflash
                        </span>
                    </span>
                    <div className="newsticker">
                        <ul className="newsticker__list">
                            <li className="newsticker__item">
                                <a
                                    href="single-post-politics.html"
                                    className="newsticker__item-url"
                                >
                                    A-HA theme is multi-purpose solution for any
                                    kind of business
                                </a>
                            </li>
                            <li className="newsticker__item">
                                <a
                                    href="single-post-1.html"
                                    className="newsticker__item-url"
                                >
                                    Satelite cost tens of millions or even
                                    hundreds of millions of dollars to build
                                </a>
                            </li>
                            <li className="newsticker__item">
                                <a
                                    href="single-post-3.html"
                                    className="newsticker__item-url"
                                >
                                    8 Hidden Costs of Starting and Running a
                                    Business
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div className="newsticker-buttons">
                        <button
                            className="newsticker-button newsticker-button--prev"
                            id="newsticker-button--prev"
                            aria-label="next article"
                        >
                            <i className="ui-arrow-left" />
                        </button>
                        <button
                            className="newsticker-button newsticker-button--next"
                            id="newsticker-button--next"
                            aria-label="previous article"
                        >
                            <i className="ui-arrow-right" />
                        </button>
                    </div>
                </div>
            </div>
        )
    }
}

export default Trending
