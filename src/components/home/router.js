import React from 'react'
import { BrowserRouter as Router, Route, Link, Switch } from 'react-router-dom'

import Index from './'
import Detail from './detail'

import Headers from '../chunks/headers'
import TopHeader from '../chunks/topHeader'
import Navigation from '../chunks/navigation'
import Trending from '../chunks/trending'
import Footer from '../chunks/footer'

function RouterHome() {
    return (
        <Router>
            <div>
                <Headers />
                <main className="main oh" id="main">
                    <TopHeader />
                    <header className="header d-lg-block d-none">
                        <div className="container">
                            <div className="flex-parent">
                                <div className="flex-child row">
                                    <Link
                                        to="/"
                                        className="logo col-md-3"
                                        id="logo-ivoox"
                                    >
                                        <img
                                            className="logo__img"
                                            src={require('../../assets/img/logo-ivoox.png')}
                                        />
                                    </Link>
                                    <div className="text-center col-md-9">
                                        <a href="#">
                                            <img
                                                className="logo__img"
                                                src={require('../../assets/img//content/placeholder_728.jpg')}
                                                alt=""
                                            />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </header>
                    <Navigation />
                    <Trending />
                    <Switch>
                        <Route exact path="/" component={Index} />
                        <Route path="/news/:id/:title" component={Detail} />
                    </Switch>
                    <Footer />
                    <div id="back-to-top">
                        <a href="#top" aria-label="Go to top">
                            <i className="ui-arrow-up" />
                        </a>
                    </div>
                </main>
            </div>
        </Router>
    )
}

export default RouterHome
