import React, { Component } from 'react'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom'

class Home extends Component {
    constructor(props) {
        super(props)
    }

    componentDidMount() {
        this.bundle()
    }

    bundle() {
        let scripts = [
            { src: './assets/js/jquery.min.js' },
            { src: './assets/js/bootstrap.min.js' },
            { src: './assets/js/easing.min.js' },
            { src: './assets/js/owl-carousel.min.js' },
            { src: './assets/js/flickity.pkgd.min.js' },
            { src: './assets/js/twitterFetcher_min.js' },
            { src: './assets/js/jquery.newsTicker.min.js' },
            { src: './assets/js/modernizr.min.js' },
            { src: './assets/js/twitterFetcher_min.js' },
            { src: './assets/js/scripts.js' }
        ]
        //Append the script element on each iteration
        scripts.map(item => {
            const script = document.createElement('script')
            script.src = item.src
            script.async = true
            document.body.appendChild(script)
        })
    }

    render() {
        const headlines = [
            {
                id: 1,
                datas: [
                    {
                        id: 1,
                        title: `'It's not a concentration camp': Bangladesh defends plan to house Rohingya on island with armed police`
                    },
                    {
                        id: 2,
                        title: `Saudi Arabia to build opera house in bid to shed conservative image, lure tourists`
                    },
                    {
                        id: 3,
                        title: `SpaceX launches experimental Starlink internet satellites`
                    },
                    {
                        id: 4,
                        title: `Canada seeks to get more women into UN peacekeeping operations`
                    },
                    {
                        id: 5,
                        title: `Ford North America president out after inappropriate behaviour`
                    },
                    {
                        id: 6,
                        title: `More than 90 Nigerian schoolgirls feared missing after Boko Haram attack video`
                    },
                    {
                        id: 7,
                        title: `Countries move too slowly against corruption, watchdog group says`
                    },
                    {
                        id: 8,
                        title: `U.S. Embassy targeted in Montenegro, attacker kills only himself`
                    },
                    {
                        id: 9,
                        title: `Amnesty says global superpowers are 'callously undermining the rights of millions'`
                    },
                    {
                        id: 10,
                        title: `U.S. Embassy targeted in Montenegro, attacker kills only himself`
                    }
                ]
            },
            {
                id: 2,
                datas: [
                    {
                        id: 1,
                        title: `'It's not a concentration camp': Bangladesh defends plan to house Rohingya on island with armed police`
                    },
                    {
                        id: 2,
                        title: `Saudi Arabia to build opera house in bid to shed conservative image, lure tourists`
                    },
                    {
                        id: 3,
                        title: `SpaceX launches experimental Starlink internet satellites`
                    },
                    {
                        id: 4,
                        title: `Canada seeks to get more women into UN peacekeeping operations`
                    },
                    {
                        id: 5,
                        title: `Ford North America president out after inappropriate behaviour`
                    },
                    {
                        id: 6,
                        title: `More than 90 Nigerian schoolgirls feared missing after Boko Haram attack video`
                    },
                    {
                        id: 7,
                        title: `Countries move too slowly against corruption, watchdog group says`
                    },
                    {
                        id: 8,
                        title: `U.S. Embassy targeted in Montenegro, attacker kills only himself`
                    },
                    {
                        id: 9,
                        title: `Amnesty says global superpowers are 'callously undermining the rights of millions'`
                    },
                    {
                        id: 10,
                        title: `U.S. Embassy targeted in Montenegro, attacker kills only himself`
                    }
                ]
            },
            {
                id: 3,
                datas: [
                    {
                        id: 1,
                        title: `'It's not a concentration camp': Bangladesh defends plan to house Rohingya on island with armed police`
                    },
                    {
                        id: 2,
                        title: `Saudi Arabia to build opera house in bid to shed conservative image, lure tourists`
                    },
                    {
                        id: 3,
                        title: `SpaceX launches experimental Starlink internet satellites`
                    },
                    {
                        id: 4,
                        title: `Canada seeks to get more women into UN peacekeeping operations`
                    },
                    {
                        id: 5,
                        title: `Ford North America president out after inappropriate behaviour`
                    },
                    {
                        id: 6,
                        title: `More than 90 Nigerian schoolgirls feared missing after Boko Haram attack video`
                    },
                    {
                        id: 7,
                        title: `Countries move too slowly against corruption, watchdog group says`
                    },
                    {
                        id: 8,
                        title: `U.S. Embassy targeted in Montenegro, attacker kills only himself`
                    },
                    {
                        id: 9,
                        title: `Amnesty says global superpowers are 'callously undermining the rights of millions'`
                    },
                    {
                        id: 10,
                        title: `U.S. Embassy targeted in Montenegro, attacker kills only himself`
                    }
                ]
            }
        ]

        const weekPopulars = [
            {
                id: 1,
                title: 'Barack Obama and Family Visit Balinese Paddy Fields',
                image: require('../../assets/img/content/post_small/post_small_5.jpg')
            },
            {
                id: 2,
                title: 'Barack Obama and Family Visit Balinese Paddy Fields',
                image: require('../../assets/img/content/post_small/post_small_5.jpg')
            },
            {
                id: 3,
                title: 'Barack Obama and Family Visit Balinese Paddy Fields',
                image: require('../../assets/img/content/post_small/post_small_5.jpg')
            },
            {
                id: 4,
                title: 'Barack Obama and Family Visit Balinese Paddy Fields',
                image: require('../../assets/img/content/post_small/post_small_5.jpg')
            },
            {
                id: 5,
                title: 'Barack Obama and Family Visit Balinese Paddy Fields',
                image: require('../../assets/img/content/post_small/post_small_5.jpg')
            },
            {
                id: 6,
                title: 'Barack Obama and Family Visit Balinese Paddy Fields',
                image: require('../../assets/img/content/post_small/post_small_5.jpg')
            },
            {
                id: 7,
                title: 'Barack Obama and Family Visit Balinese Paddy Fields',
                image: require('../../assets/img/content/post_small/post_small_5.jpg')
            },
            {
                id: 8,
                title: 'Barack Obama and Family Visit Balinese Paddy Fields',
                image: require('../../assets/img/content/post_small/post_small_5.jpg')
            }
        ]

        const insights = [
            {
                id: 1,
                title: `Lose These 12 Bad Habits If You're Serious About Becoming a Millionaire`,
                image: require('../../assets/img/content/grid/grid_post_3.jpg'),
                author: {
                    id: 1,
                    name: 'Muhamad Yusup Hamdani'
                },
                description: 'iPrice Group report offers insights on daily e-commerce activity in the ...',
                createdAt: 'Jan 21, 2018'
            }
        ]

        return (
            <div className="main-container container" id="main-container">
                {/* Content */}
                <div className="row row-20">
                    {/* Posts */}
                    <div className="col-lg-6 order-lg-2">
                        <section className="section mb-16">
                            <article className="entry thumb thumb--size-3 thumb--mb-20">
                                <div className="entry__img-holder thumb__img-holder" style={{backgroundImage: `url(${require("../../assets/img/content/hero/hero_post_5.jpg")})`}}>
                                    <div className="bottom-gradient"></div>
                                    <div className="thumb-text-holder thumb-text-holder--2">
                                        <ul className="entry__meta">
                                            <li>
                                                <a href="#" className="entry__meta-category">politics</a>
                                            </li>
                                        </ul>
                                        <h2 className="thumb-entry-title">
                                            <Link to={{ pathname: `/news/a-1/${'Trump-endorses-raising-minimum-age-for-more-weapons-revives-idea-of-arming-teachers'}`, search: "?tag_from=yahoo" }} className="nav-link" className="nav-link">
                                                Trump endorses raising minimum age for more weapons, revives idea of arming teachers
                                            </Link>
                                        </h2>
                                        <ul className="entry__meta">
                                            <li className="entry__meta-views">
                                                <i className="ui-eye"></i>
                                                <span>1356</span>
                                            </li>
                                            <li className="entry__meta-comments">
                                                <a href="#">
                                                    <i className="ui-chat-empty"></i>13
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </article>
                            <div className="row row-20">
                                <div className="col-md-6">
                                    <article className="entry card">
                                        <div className="entry__img-holder card__img-holder">
                                            <a href="single-post.html">
                                                <div className="thumb-container thumb-70">
                                                    <img className="logo__img" src={require('../../assets/img/content/grid/grid_post_2.jpg')} className="entry__img lazyload" alt="" />
                                                </div>
                                            </a>
                                            <a href="#" className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner">Politics</a>
                                        </div>

                                        <div className="entry__body card__body">
                                            <div className="entry__header">

                                                <h2 className="entry__title">
                                                    <a href="single-post.html">3 Things You Can Do to Get Your Customers Talking About Your Business</a>
                                                </h2>
                                                <ul className="entry__meta">
                                                    <li className="entry__meta-author">
                                                        <span>by</span>
                                                        <a href="#">DeoThemes</a>
                                                    </li>
                                                    <li className="entry__meta-date">
                                                        Jan 21, 2018
                                                    </li>
                                                </ul>
                                            </div>
                                            <div className="entry__excerpt">
                                                <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                                            </div>
                                        </div>
                                    </article>
                                </div>

                                <div className="col-md-6">
                                    <article className="entry card">
                                        <div className="entry__img-holder card__img-holder">
                                            <a href="single-post.html">
                                                <div className="thumb-container thumb-70">
                                                    <img className="logo__img" src={require('../../assets/img/content/grid/grid_post_2.jpg')} className="entry__img lazyload" alt="" />
                                                </div>
                                            </a>
                                            <a href="#" className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner">Politics</a>
                                        </div>

                                        <div className="entry__body card__body">
                                            <div className="entry__header">

                                                <h2 className="entry__title">
                                                    <a href="single-post.html">3 Things You Can Do to Get Your Customers Talking About Your Business</a>
                                                </h2>
                                                <ul className="entry__meta">
                                                    <li className="entry__meta-author">
                                                        <span>by</span>
                                                        <a href="#">DeoThemes</a>
                                                    </li>
                                                    <li className="entry__meta-date">
                                                        Jan 21, 2018
                                                    </li>
                                                </ul>
                                            </div>
                                            <div className="entry__excerpt">
                                                <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                                            </div>
                                        </div>
                                    </article>
                                </div>

                            </div>
                        </section>
                    </div>
                    {/* end posts */}


                    <aside className="col-lg-3 sidebar order-lg-1">
                        <aside className="widget widget-headlines">
                            <h4 className="widget-title">More Headlines</h4>
                            <div id="owl-headlines" className="owl-carousel owl-theme">
                            {headlines.map((headline, headlineIndex) =>
                                <div className="owl-item" key={headlineIndex}>
                                    <ul className="post-list-small post-list-small--1">
                                        {headline.datas.map((headlineItem, headlineItemIndex) => 
                                            <li className="post-list-small__item" key={headlineItemIndex}>
                                                <article className="post-list-small__entry clearfix">
                                                    <div className="post-list-small__body">
                                                        <h3 className="post-list-small__entry-title">
                                                            <a href="single-post-politics.html">{headlineItem.title}</a>
                                                        </h3>
                                                    </div>
                                                </article>
                                            </li>
                                        )}
                                    </ul>
                                </div>
                            )}
                            </div>
                            <div className="owl-custom-nav text-center">
                                <button className="owl-custom-nav__btn owl-custom-nav__btn--prev" aria-label="previous slide">
                                    <i className="ui-arrow-left"></i>
                                    <span className="owl-custom-nav__label">Prev</span>
                                </button>
                                <button className="owl-custom-nav__btn owl-custom-nav__btn--next" aria-label="next slide">
                                    <span className="owl-custom-nav__label">Next</span>
                                    <i className="ui-arrow-right"></i>
                                </button>
                            </div>
                        </aside>
                    </aside>


                    {/* Sidebar */}
                    <aside className="col-lg-3 sidebar order-lg-3">
                        {/* Widget Popular Posts */}
                        <aside className="widget widget-popular-posts">
                            <h4 className="widget-title">Weekly Popular</h4>
                            <ul className="post-list-small post-list-small--1">
                                {weekPopulars.map((weekPopular, weekPopularIndex) => 
                                    <li className="post-list-small__item" key={weekPopularIndex}>
                                        <article className="post-list-small__entry clearfix">
                                            <div className="post-list-small__img-holder">
                                                <div className="thumb-container thumb-80">
                                                    <a href="single-post-politics.html">
                                                        <img className="logo__img" src={weekPopular.image} className="entry__img lazyload" alt="" />
                                                    </a>
                                                </div>
                                            </div>
                                            <div className="post-list-small__body">
                                                <h3 className="post-list-small__entry-title">
                                                    <a href="single-post-politics.html">{weekPopular.title}</a>
                                                </h3>
                                            </div>
                                        </article>
                                    </li>
                                )}
                            </ul>
                        </aside>
                    </aside>
                </div>
                <hr />
                <div className="text-center">
                    <a href="#">
                        <img className="logo__img" src={require('../../assets/img/content/placeholder_728.jpg')} />
                    </a>
                </div>
                <hr />
                <section className="section mb-24">
                    <div className="title-wrap title-wrap--line title-wrap--pr">
                        <h3 className="section-title">insight &amp; analysis</h3>
                    </div>
                    <div id="owl-posts" className="owl-carousel owl-theme owl-carousel--arrows-outside">
                        {insights.map((insight, insightIndex) =>
                            <article className="entry card" key={insightIndex}>
                                <div className="entry__img-holder card__img-holder">
                                    <a href="single-post.html">
                                        <div className="thumb-container thumb-70">
                                            <img className="logo__img" src={insight.image} className="entry__img lazyload" alt="" />
                                        </div>
                                    </a>
                                    <a href="#" className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner entry__meta-category--orange">mining</a>
                                </div>
                                <div className="entry__body card__body">
                                    <div className="entry__header">

                                        <h2 className="entry__title">
                                            <a href="single-post.html">{insight.title}</a>
                                        </h2>
                                        <ul className="entry__meta">
                                            <li className="entry__meta-author">
                                                <span>by</span>{` `}
                                                <a href="#">{insight.author.name}</a>
                                            </li>
                                            <li className="entry__meta-date">
                                                {insight.createdAt}
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="entry__excerpt">
                                        <p>{insight.description}</p>
                                    </div>
                                </div>
                            </article>
                        )}
                    </div>
                </section>
                <hr />
                <section className="section mb-24">
                    <div className="title-wrap title-wrap--line">
                        <h3 className="section-title">HEADLINE</h3>
                    </div>
                    <div className="row row-20">
                        <div className="col-md-6">
                            <article className="entry thumb thumb--size-3 thumb--mb-20">
                                <div className="entry__img-holder thumb__img-holder" style={{backgroundImage: `url(${require("../../assets/img/content/hero/hero_post_5.jpg")})`}}>
                                    <div className="bottom-gradient"></div>
                                    <div className="thumb-text-holder thumb-text-holder--2">
                                        <ul className="entry__meta">
                                            <li>
                                                <a href="#" className="entry__meta-category">politics</a>
                                            </li>
                                        </ul>
                                        <h2 className="thumb-entry-title">
                                            <a href="single-post-politics.html">Trump endorses raising minimum age for more weapons, revives idea of arming teachers</a>
                                        </h2>
                                        <ul className="entry__meta">
                                            <li className="entry__meta-views">
                                                <i className="ui-eye"></i>
                                                <span>1356</span>
                                            </li>
                                            <li className="entry__meta-comments">
                                                <a href="#">
                                                    <i className="ui-chat-empty"></i>13
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <a href="single-post-politics.html" className="thumb-url"></a>
                                </div>
                            </article>
                        </div>

                        <div className="col-md-3">
                            <article className="entry card">
                                <div className="entry__img-holder card__img-holder">
                                    <a href="single-post.html">
                                        <div className="thumb-container thumb-70">
                                            {/* <img data-src="img/content/grid/grid_post_2.jpg" src="img/empty.png" className="entry__img lazyload" alt="" /> */}
                                            <img className="logo__img" src={require('../../assets/img/content/grid/grid_post_2.jpg')} className="entry__img lazyload" alt="" />
                                        </div>
                                    </a>
                                    <a href="#" className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner">Politics</a>
                                </div>

                                <div className="entry__body card__body">
                                    <div className="entry__header">

                                        <h2 className="entry__title">
                                            <a href="single-post.html">3 Things You Can Do to Get Your Customers Talking About Your Business</a>
                                        </h2>
                                        <ul className="entry__meta">
                                            <li className="entry__meta-author">
                                                <span>by</span>
                                                <a href="#">DeoThemes</a>
                                            </li>
                                            <li className="entry__meta-date">
                                                Jan 21, 2018
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="entry__excerpt">
                                        <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                                    </div>
                                </div>
                            </article>
                        </div>

                        <div className="col-md-3">
                            <article className="entry card">
                                <div className="entry__img-holder card__img-holder">
                                    <a href="single-post.html">
                                        <div className="thumb-container thumb-70">
                                            {/* <img data-src="img/content/grid/grid_post_2.jpg" src="img/empty.png" className="entry__img lazyload" alt="" /> */}
                                            <img className="logo__img" src={require('../../assets/img/content/grid/grid_post_2.jpg')} className="entry__img lazyload" alt="" />
                                        </div>
                                    </a>
                                    <a href="#" className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner">Politics</a>
                                </div>

                                <div className="entry__body card__body">
                                    <div className="entry__header">

                                        <h2 className="entry__title">
                                            <a href="single-post.html">3 Things You Can Do to Get Your Customers Talking About Your Business</a>
                                        </h2>
                                        <ul className="entry__meta">
                                            <li className="entry__meta-author">
                                                <span>by</span>
                                                <a href="#">DeoThemes</a>
                                            </li>
                                            <li className="entry__meta-date">
                                                Jan 21, 2018
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="entry__excerpt">
                                        <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                                    </div>
                                </div>
                            </article>
                        </div>

                    </div>
                </section>

                <hr />

                <section className="section mb-24">
                    <div className="title-wrap title-wrap--line">
                        <h3 className="section-title">VOXPOP</h3>
                    </div>
                    <div className="row row-20">

                        <div className="col-md-3">
                            <article className="entry card">
                                <div className="entry__img-holder card__img-holder">
                                    <a href="single-post.html">
                                        <div className="thumb-container thumb-70">
                                            {/* <img data-src="img/content/grid/grid_post_2.jpg" src="img/empty.png" className="entry__img lazyload" alt="" /> */}
                                            <img className="logo__img" src={require('../../assets/img/content/grid/grid_post_2.jpg')} className="entry__img lazyload" alt="" />
                                        </div>
                                    </a>
                                    <a href="#" className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner">Politics</a>
                                </div>

                                <div className="entry__body card__body">
                                    <div className="entry__header">

                                        <h2 className="entry__title">
                                            <a href="single-post.html">3 Things You Can Do to Get Your Customers Talking About Your Business</a>
                                        </h2>
                                        <ul className="entry__meta">
                                            <li className="entry__meta-author">
                                                <span>by</span>
                                                <a href="#">DeoThemes</a>
                                            </li>
                                            <li className="entry__meta-date">
                                                Jan 21, 2018
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="entry__excerpt">
                                        <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                                    </div>
                                </div>
                            </article>
                        </div>

                        <div className="col-md-3">
                            <article className="entry card">
                                <div className="entry__img-holder card__img-holder">
                                    <a href="single-post.html">
                                        <div className="thumb-container thumb-70">
                                            {/* <img data-src="img/content/grid/grid_post_2.jpg" src="img/empty.png" className="entry__img lazyload" alt="" /> */}
                                            <img className="logo__img" src={require('../../assets/img/content/grid/grid_post_2.jpg')} className="entry__img lazyload" alt="" />
                                        </div>
                                    </a>
                                    <a href="#" className="entry__meta-category entry__meta-category--label entry__meta-category--align-in-corner">Politics</a>
                                </div>

                                <div className="entry__body card__body">
                                    <div className="entry__header">

                                        <h2 className="entry__title">
                                            <a href="single-post.html">3 Things You Can Do to Get Your Customers Talking About Your Business</a>
                                        </h2>
                                        <ul className="entry__meta">
                                            <li className="entry__meta-author">
                                                <span>by</span>
                                                <a href="#">DeoThemes</a>
                                            </li>
                                            <li className="entry__meta-date">
                                                Jan 21, 2018
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="entry__excerpt">
                                        <p>iPrice Group report offers insights on daily e-commerce activity in the ...</p>
                                    </div>
                                </div>
                            </article>
                        </div>

                        <div className="col-md-6">
                            <article className="entry thumb thumb--size-3 thumb--mb-20">
                                <div className="entry__img-holder thumb__img-holder" style={{backgroundImage: `url(${require("../../assets/img/content/hero/hero_post_5.jpg")})`}}>
                                    <div className="bottom-gradient"></div>
                                    <div className="thumb-text-holder thumb-text-holder--2">
                                        <ul className="entry__meta">
                                            <li>
                                                <a href="#" className="entry__meta-category">politics</a>
                                            </li>
                                        </ul>
                                        <h2 className="thumb-entry-title">
                                            <a href="single-post-politics.html">Trump endorses raising minimum age for more weapons, revives idea of arming teachers</a>
                                        </h2>
                                        <ul className="entry__meta">
                                            <li className="entry__meta-views">
                                                <i className="ui-eye"></i>
                                                <span>1356</span>
                                            </li>
                                            <li className="entry__meta-comments">
                                                <a href="#">
                                                    <i className="ui-chat-empty"></i>13
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                    <a href="single-post-politics.html" className="thumb-url"></a>
                                </div>
                            </article>
                        </div>
                    </div>
                </section>

                <hr />

                <section className="section mb-24">

                    <div className="title-wrap title-wrap--line">
                        <h3 className="section-title">FOTO</h3>
                    </div>
                    <div className="owl-carousel owl-theme">
                        <img className="owl-lazy" data-src="https://placehold.it/350x450&text=1" data-src-retina="https://placehold.it/350x250&text=1-retina" alt="" />
                        <img className="owl-lazy" data-src="https://placehold.it/350x650&text=2" data-src-retina="https://placehold.it/350x250&text=2-retina" alt="" />
                        <picture>
                            <source className="owl-lazy" media="(min-width: 650px)" data-srcset="https://placehold.it/350x250&text=3-large" />
                            <source className="owl-lazy" media="(min-width: 350px)" data-srcset="https://placehold.it/350x250&text=3-medium" />
                            <img className="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="" />
                        </picture>
                        <img className="owl-lazy" data-src="https://placehold.it/350x250&text=4" alt="" />
                        <img className="owl-lazy" data-src="https://placehold.it/350x250&text=5" alt="" />
                        <img className="owl-lazy" data-src="https://placehold.it/350x250&text=6" alt="" />
                        <img className="owl-lazy" data-src="https://placehold.it/350x250&text=7" alt="" />
                        <img className="owl-lazy" data-src="https://placehold.it/350x250&text=8" alt="" />
                        <img className="owl-lazy" data-src="https://placehold.it/350x400&text=9" alt="" />
                        <img className="owl-lazy" data-src="https://placehold.it/350x400&text=10" alt="" />
                        <img className="owl-lazy" data-src="https://placehold.it/350x450&text=11" alt="" />
                    </div>
                </section>

                <hr />

                <section className="section mb-24">

                    <div className="title-wrap title-wrap--line">
                        <h3 className="section-title">VIDEO</h3>
                    </div>
                    <div className="owl-carousel owl-theme">
                        <img className="owl-lazy" data-src="https://placehold.it/350x450&text=1" data-src-retina="https://placehold.it/350x250&text=1-retina" alt="" />
                        <img className="owl-lazy" data-src="https://placehold.it/350x650&text=2" data-src-retina="https://placehold.it/350x250&text=2-retina" alt="" />
                        <picture>
                            <source className="owl-lazy" media="(min-width: 650px)" data-srcset="https://placehold.it/350x250&text=3-large" />
                            <source className="owl-lazy" media="(min-width: 350px)" data-srcset="https://placehold.it/350x250&text=3-medium" />
                            <img className="owl-lazy" data-src="https://placehold.it/350x250&text=3-fallback" alt="" />
                        </picture>
                        <img className="owl-lazy" data-src="https://placehold.it/350x250&text=4" alt="" />
                        <img className="owl-lazy" data-src="https://placehold.it/350x250&text=5" alt="" />
                        <img className="owl-lazy" data-src="https://placehold.it/350x250&text=6" alt="" />
                        <img className="owl-lazy" data-src="https://placehold.it/350x250&text=7" alt="" />
                        <img className="owl-lazy" data-src="https://placehold.it/350x250&text=8" alt="" />
                        <img className="owl-lazy" data-src="https://placehold.it/350x400&text=9" alt="" />
                        <img className="owl-lazy" data-src="https://placehold.it/350x400&text=10" alt="" />
                        <img className="owl-lazy" data-src="https://placehold.it/350x450&text=11" alt="" />
                    </div>
                </section>
            </div>
        )
    }
}

export default Home